import React from 'react';
import { Menu } from 'kinedu-react-components';
import { APP_SECTIONS_PATHS, menuItems, menuRoutes } from './routes';
import {
  Route,
  BrowserRouter as Router,
  Switch,
} from 'react-router-dom';

// const RouterContext = React.createContext(null);

function RouteMenu() {

  return (
    <Router>
      <Route>
        {(routeProps) => {
          const { history, location } = routeProps;
          // currentParamsRef.current = parse(location.search);
          return (
            <Switch>
              <Menu
                appName="Kinedu Experience"
                items={menuItems.map((item) => ({
                  ...item,
                  title: item.title,
                }))}
                activeRoute={location.pathname}
                onItemClicked={(item) => history.push(item.route)}
                onEditClicked={() => history.push(APP_SECTIONS_PATHS.USER_PROFILE)}
                content={() => (
                  <Switch>
                    {menuRoutes.map((route) => {
                      const Component = route.component;
                      return (
                        <Route
                          key={route.path}
                          exact={route.exact}
                          path={route.path}
                          render={(renderProps) => (
                            <Component {...renderProps} />
                          )}
                        />
                      );
                    })}
                  </Switch>
                )}
              />
            </Switch>
          );
        }}
      </Route>
    </Router>
  );
}

export default RouteMenu;
