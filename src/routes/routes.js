import ExperimentWorkflow from '../views/ExperimentWorkflow';

export const APP_SECTIONS_PATHS = {
  HOME: '/',
  EXPERIMENT_WORKFLOW: '/experiment-workflow/',
  EXPERIMENTS: '/experiments',
  EXPERIENCES: '/experiences',
  CUSTOMERS: '/customers',
  USER_PROFILE: '/user-profile',
};

export const menuItems = [
  {
    title: 'Home',
    route: APP_SECTIONS_PATHS.HOME,
  },
  {
    title: 'Experiment Workflow',
    route: APP_SECTIONS_PATHS.EXPERIMENT_WORKFLOW,
  },
];

export const menuRoutes = [
  {
    path: APP_SECTIONS_PATHS.HOME,
    component: ExperimentWorkflow,
    exact: true,
    meta: {},
  },
  {
    path: APP_SECTIONS_PATHS.EXPERIMENT_WORKFLOW,
    component: ExperimentWorkflow,
    exact: false,
    meta: {},
  },
];
