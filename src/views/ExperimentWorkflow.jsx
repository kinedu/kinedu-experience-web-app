import React from 'react';
import { Switcher } from 'kinedu-react-components';
import styled from 'styled-components';

const Header = styled.div`
  width: 100%;
  height: 100px;
`;

function ExperimentWorkflow() {
  return (
    <div>
      <Header>
        {`Hello_${1}`}
        <Switcher theme="Pill" />
      </Header>
    </div>
  );
}

export default ExperimentWorkflow;
