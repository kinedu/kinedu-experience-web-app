import React from 'react';
import './App.css';
import RouteMenu from './routes';

function App() {
  return (
    <div className="App">
      <RouteMenu />
    </div>
  );
}

export default App;
