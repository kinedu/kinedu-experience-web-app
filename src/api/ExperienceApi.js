import axios from 'axios';
import { EXPERIENCE_API_URL } from '../conifg';

const ExperienceApi = axios.create({
  baseURL: EXPERIENCE_API_URL,
  timeout: 30000,
  headers: {
    // Authorization: 'Bearer',
  },
});

export default ExperienceApi;
